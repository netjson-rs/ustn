# `ustn`: UISP / UniFi State to NetJSON

NYC Mesh uses [UISP](https://uisp.ui.com/) to monitor the state of a majority of
the directional and sector antennas on its network. It also uses [UniFi Network
Controllers](https://www.ui.com/download-software/) to manage end-user APs
deployed at various locations. `ustn` attempts to help bridge the "information
gap" between UISP, UniFi and other Mesh internal systems by collecting useful
information into a single place with a standardised format.

`ustn` will download information from all sites and devices from both UISP and
the UniFi controller and convert link and device information into a [NetJSON
Network Graph](https://netjson.org/rfc.html#NetworkGraph). It may be used as a
standalone binary or a library to be integrated with other infrastructure.

## Usage

`ustn` doesn't ship with NYC Mesh credentials. If shared credentials that may be
changed in the future works for you, talk to @mz, otherwise @Olivier is the
person most involved with UISP and the UniFi controllers, you can ping him if
you'd like your own personal accounts.

### Configuration

Credentials are saved in a TOML file like this:

```toml
[[uisp]]
# UISP url, don't forget the trailing slash. Using unms.mesh will also work if
# you use the 10.10.10.10 DNS server.
uri = "https://10.70.76.21/"
username = "example_user"
password = "example-pässword!"
# Maybe we'll have certs signed by a default CA at some point in the future…
# but right now we don't
invalid_certs = true

[[unifi]]
# UniFi controller address. Use 10.70.95.63 if mesh DNS doesn't work for you
uri = "https://unifi.mesh:8443/"
## Hardware based UniFi controllers use different URIs for its API but is
## compatible otherwise
# hardware = false
username = "example_user"
password = "example-pässword!"
invalid_certs = true

# NYC Mesh currently uses another UniFi controller, a hardware one, at Grand St
[[unifi]]
# UniFi controller address. Use 10.70.95.63 if mesh DNS doesn't work for you
uri = "https://10.97.227.184/"
hardware = true
username = "example_user"
password = "example-pässword!"
invalid_certs = true
```

For the standalone tool, this file is expected to be named `ustn.conf` in
`$HOME/.config/ustn/` (Linux), `/Users/$user/Library/Application Support/ustn/`
(Mac), or `C:\Users\$user\AppData\Roaming\ustn\` (Windows). `ustn` will
automatically add authentication token and cookie files to the same folder, so
as to not log in again and again, riddling the logs with unnecessary
authentication entries.

### Building & running

If you'd like to use `ustn` as a standalone tool, run `cargo build
--features=standalone` or `cargo build --features=standalone --target=release`
if you'd like to build it without debug symbols.

Once you have a configuration file in place, you can run it either with
`cargo run --features=standalone` or enter the target folder created after
building and running either `./debug/ustn` or `./release/ustn`.

Here's currently supported command line arguments:

```shell
$ ustn --help
ustn 0.3.0

USAGE:
    ustn [FLAGS] [OPTIONS]

FLAGS:
        --all         Include everything, including non-Ubiquiti devices and inactive devices
        --debug       Show parsed data structures rather than converting them into NetJSON
    -h, --help        Prints help information
        --uisp        Only return UISP graph
        --unifi       Only return UniFi graph
    -V, --version     Prints version information
        --wireless    Only include Ubiquiti wireless devices

OPTIONS:
    -c, --config <config>    Folder for configuration files [default: ~/.config/ustn/]
        --filter <filter>    Filter output. UISP filters by AP/sector. "nycmesh-1340-west1" will only show devices
                             connected to that sector. / UniFi filters by sites and names of devices. "1340" will show
                             devices for the 1340 site but also devices with 1340 in their name
```

By default it will just return a NetJSON Network Graph of all Ubiquiti and UniFi
network devices that are currently connected.

## Getting started with Rust!

[Installing Rust](https://rustup.rs/) is super easy! Once you've got the
toolchain installed, you can use the section above to start building and
running this little tool!

## Bits and bobs specific to NYC Mesh

`ustn` was built as a general purpose tool and does not rely on anything
specific to NYC Mesh. If you're interested in tools that help with NYC Mesh
support and network engineering, check out the `nycmesh-utils`, currently
available [here](http://gitlab.mesh/mz/nycmesh-utils/),
[here](http://10.70.123.5/mz/nycmesh-utils/) and
[here](https://ghom.niij.org/nycmesh/nycmesh-utils/).
