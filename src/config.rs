use crate::*;

use std::fs::{metadata, write, File};
use std::io::{Read, Write};
use std::time::SystemTime;

#[derive(Deserialize)]
pub struct Config {
    pub uisp: Option<Vec<uisp::Uisp>>,
    pub unifi: Option<Vec<unifi::UniFi>>,
}

#[inline]
fn uri_to_filename(uri: &str, suffix: &str) -> String {
    format!(
        "{}.{}",
        uri.trim_start_matches("http")
            .trim_start_matches('s')
            .trim_start_matches("://")
            .splitn(2, '/')
            .collect::<Vec<_>>()[0]
            .replace(':', "_"),
        suffix
    )
}

#[cfg(feature = "standalone")]
pub fn get_config(config_folder: &Option<PathBuf>) -> Result<Config> {
    let mut path_default;

    config_from_path(match config_folder {
        None => match dirs::config_dir() {
            Some(c) => {
                path_default = c;
                path_default.push("ustn");
                Ok(path_default.as_path())
            }
            None => Err(Box::new(Error::Config(
                "Cannot determine user configuration directory",
            ))),
        },
        Some(c) => Ok(c.as_path()),
    }?)
}

#[cfg(not(feature = "standalone"))]
pub fn get_config(config_folder: &Path) -> Result<Config> {
    config_from_path(config_folder)
}

fn config_from_path(config_folder: &Path) -> Result<Config> {
    let mut config_file: PathBuf = config_folder.into();
    config_file.push("ustn.conf");

    let mut buf = Vec::new();
    let mut config_file = File::open(&config_file)?;
    config_file.read_to_end(&mut buf)?;
    let mut config: Config = toml::from_slice(&buf)?;

    // TODO: Make the following 4 blocks into a macro since they follow the same structure
    if let Some(uisp) = &mut config.uisp {
        for controller in uisp {
            controller.uri = format!("{}v2.1/", controller.uri);
            let mut token_file: PathBuf = config_folder.into();
            token_file.push(uri_to_filename(&controller.uri, "token"));
            if token_file.exists() {
                let since = SystemTime::now().duration_since(metadata(&token_file)?.modified()?)?;
                if since.as_secs() < 604800 {
                    buf = Vec::new();
                    let mut token_file = File::open(&token_file)?;
                    token_file.read_to_end(&mut buf)?;
                    let buf = std::str::from_utf8(&buf)?.trim().as_bytes();
                    controller.token = Some(buf.to_vec());
                }
            }
            controller.token_path = Some(token_file);
        }
    }

    if let Some(unifi) = &mut config.unifi {
        for controller in unifi {
            let mut cookies_file: PathBuf = config_folder.into();
            cookies_file.push(uri_to_filename(&controller.uri, "cookies"));
            if cookies_file.exists() {
                let since =
                    SystemTime::now().duration_since(metadata(&cookies_file)?.modified()?)?;
                if since.as_secs() < 86400 {
                    buf = Vec::new();
                    let mut cookies_file = File::open(&cookies_file)?;
                    cookies_file.read_to_end(&mut buf)?;
                    let buf = std::str::from_utf8(&buf)?.trim().as_bytes();
                    controller.cookies = Some(buf.to_vec());
                }
            }
            controller.cookies_path = Some(cookies_file);
        }
    }

    Ok(config)
}

pub fn set_token(token: &[u8], uisp: &mut uisp::Uisp) -> Result<()> {
    uisp.token = Some(token.to_owned());
    match &uisp.token_path {
        Some(p) => {
            let mut buf = token.to_owned();
            writeln!(&mut buf)?;
            write(p, buf)?;
        }
        None => return Err(Box::new(Error::Config("token_path needs to be set first"))),
    }
    Ok(())
}

pub fn set_cookies(cookies: &[u8], unifi: &mut unifi::UniFi) -> Result<()> {
    unifi.cookies = Some(cookies.to_owned());
    match &unifi.cookies_path {
        Some(p) => {
            let mut buf = cookies.to_owned();
            writeln!(&mut buf)?;
            write(p, buf)?;
        }
        None => {
            return Err(Box::new(Error::Config(
                "cookies_path needs to be set first",
            )))
        }
    }
    Ok(())
}
