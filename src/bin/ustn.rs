use std::path::PathBuf;
use std::process;

use structopt::StructOpt;
use ustn::*;

#[derive(StructOpt, Debug)]
struct Opt {
    #[structopt(short = "c", long, parse(from_os_str))]
    /// Folder for configuration files [default: ~/.config/ustn/]
    config: Option<PathBuf>,
    /// Filter output. UISP filters by AP/sector. "nycmesh-1340-west1" will only show devices
    /// connected to that sector. / UniFi filters by sites and names of devices. "1340" will show
    /// devices for the 1340 site but also devices with 1340 in their name
    #[structopt(long)]
    filter: Option<String>,
    /// Show parsed data structures rather than converting them into NetJSON
    #[structopt(long)]
    debug: bool,
    /// Only include Ubiquiti wireless devices
    #[structopt(long)]
    wireless: bool,
    /// Include everything, including non-Ubiquiti devices and inactive devices
    #[structopt(long)]
    all: bool,
    /// Only return UniFi graph
    #[structopt(long)]
    unifi: bool,
    /// Only return UISP graph
    #[structopt(long)]
    uisp: bool,
}

#[tokio::main]
async fn main() -> Result<()> {
    let opt = Opt::from_args();

    if opt.uisp && opt.unifi {
        eprintln!("Combining --uisp and --unifi yields no output.");
        process::exit(1);
    }

    let mut config = config::get_config(&opt.config)?;

    let graph = grab_custom(
        &mut config,
        opt.uisp,
        opt.unifi,
        opt.all,
        opt.wireless,
        &opt.filter,
    )
    .await;

    match graph {
        Ok(graph) => {
            if opt.debug {
                println!("{:#?}", graph);
                process::exit(0);
            }
            println!("{}", serde_json::to_string(&graph)?);
        }
        Err(e) => {
            match e.downcast_ref() {
                Some(&Error::Empty) => eprintln!("There is no graph to export export 😢"),
                _ => eprintln!("{}", e),
            }
            process::exit(1);
        }
    }

    Ok(())
}
