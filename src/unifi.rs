use std::net::Ipv4Addr;

use mac_address::MacAddress;
use serde_json::json;

use crate::*;

#[derive(Deserialize)]
pub struct UniFi {
    pub uri: String,
    pub hardware: Option<bool>,
    pub invalid_certs: Option<bool>,
    pub username: String,
    pub password: String,
    #[serde(skip)]
    pub cookies: Option<Vec<u8>>,
    pub cookies_path: Option<PathBuf>,
    #[serde(skip)]
    client: Option<reqwest::Client>,
    #[serde(skip)]
    actual_uri: Option<String>,
}

#[async_trait]
impl DataSource for UniFi {
    async fn login(&mut self, retry: bool) -> Result<()> {
        // We don't need to do this again if we already have a client and aren't retrying
        if let (false, Some(_)) = (retry, self.client.as_ref()) {
            return Ok(());
        }

        let mut headers = header::HeaderMap::new();
        headers.insert(
            reqwest::header::ACCEPT,
            header::HeaderValue::from_static("application/json"),
        );
        match (&self.cookies, retry) {
            (Some(b), false) => headers.insert("Cookie", header::HeaderValue::from_bytes(&b)?),
            _ => {
                let mut login_uri = self.uri.clone();
                // Hardware based UniFi controllers (for example cloudkey or Dream Machine Pro) use
                // different URIs but the API is the same
                match self.hardware {
                    Some(true) => login_uri.push_str("api/auth/login"),
                    _ => login_uri.push_str("api/login"),
                }

                let login = build_client(self, headers.clone())
                    .await?
                    .post(&login_uri)
                    .json(&json!({
                        "password": &self.password,
                        "username": &self.username,
                        "remember": true
                    }))
                    .send()
                    .await?;

                if login.status() != reqwest::StatusCode::OK {
                    return Err(Box::new(Error::Auth(login_uri)));
                }
                let mut cookies: Vec<u8> = vec![];
                for cookie in login.headers().get_all("set-cookie") {
                    match cookie.to_str() {
                        Ok(b) => {
                            if let Some(i) = b.find(';') {
                                cookies.extend_from_slice(&cookie.as_bytes()[..i + 1]);
                                cookies.push(b' ');
                            } else {
                                return Err(Box::new(Error::Unexpected(b.into(), login_uri)));
                            }
                        }
                        Err(e) => {
                            return Err(Box::new(Error::Unexpected(format!("{}", e), login_uri)))
                        }
                    }
                }
                if cookies.is_empty() {
                    return Err(Box::new(Error::Unexpected(
                        "200 OK but empty cookie?!".into(),
                        login_uri,
                    )));
                }
                let end: &[_] = &[' ', ';'];
                let cookies = String::from_utf8(cookies)?
                    .trim_end_matches(end)
                    .as_bytes()
                    .to_owned();
                config::set_cookies(&cookies, self)?;
                headers.insert("Cookie", header::HeaderValue::from_bytes(&cookies)?)
            }
        };

        // Might be overkill, but only set "actual_uri" if it's necessary
        match (self.actual_uri.as_mut(), self.hardware) {
            (None, Some(true)) => {
                let mut hardware_uri = self.uri.clone();
                hardware_uri.push_str("proxy/network/");
                self.actual_uri = Some(hardware_uri);
            }
            (Some(_), _) => {}
            _ => self.actual_uri = Some(self.uri.clone()),
        }

        self.client = Some(build_client(self, headers).await?);
        Ok(())
    }

    async fn get_devices(&self) -> Result<Devices> {
        let client = self
            .client
            .as_ref()
            .ok_or_else(|| Error::Auth(self.uri.clone()))?;

        // If we are logged in, actual_uri is Some
        let uri = self.actual_uri.as_ref().unwrap().clone();

        // UniFi does not support getting information from all devices at once, so we need to query
        // sites before we get the respective devices
        let mut sites_uri = uri.clone();
        sites_uri.push_str("api/self/sites");

        let response = client.get(&sites_uri).send().await?;

        if response.status() != reqwest::StatusCode::OK {
            return Err(Box::new(Error::Auth(sites_uri)));
        }

        let response_text = &response.text().await?;

        let response_struct: Response = serde_json::from_str(response_text)
            .map_err(|e| Error::Deserialize(e, sites_uri.clone(), response_text.clone()))?;

        let mut devices: Vec<Device> = vec![];

        if let Data::Sites(sites) = response_struct.data {
            for site in &sites {
                let mut device_uri = uri.clone();
                device_uri.push_str("api/s/");
                device_uri.push_str(&site.name);
                device_uri.push_str("/stat/device");
                let device_response = client.get(&device_uri).send().await?;
                let device_response_text = &device_response.text().await?;

                let device_response_parsed: Response = serde_json::from_str(device_response_text)
                    .map_err(|e| {
                    Error::Deserialize(e, device_uri.clone(), device_response_text.clone())
                })?;

                if let Data::Devices(site_devices) = device_response_parsed.data {
                    devices.extend(
                        site_devices
                            .into_iter()
                            .map(|mut d| {
                                d.site = Some(site.desc.clone());
                                d
                            })
                            .collect::<Vec<Device>>(),
                    );
                } else {
                    return Err(Box::new(Error::Unexpected(
                        format!("{:?}", device_response_parsed.meta),
                        device_uri,
                    )));
                }
            }
        } else {
            return Err(Box::new(Error::Unexpected(
                format!("{:?}", response_struct.meta),
                sites_uri.clone(),
            )));
        }

        Ok(Devices::UniFi(devices))
    }

    fn certs(&self) -> &Option<bool> {
        &self.invalid_certs
    }
}

fn deserialize_err_to_default<'de, D, T>(d: D) -> std::result::Result<T, D::Error>
where
    D: serde::de::Deserializer<'de>,
    T: serde::Deserialize<'de> + Default,
{
    Deserialize::deserialize(d).or(Ok(T::default()))
}

#[derive(Debug, Deserialize)]
struct Response {
    data: Data,
    meta: Meta,
}

#[derive(Debug, Deserialize)]
#[serde(untagged)]
enum Data {
    Devices(Vec<Device>),
    Sites(Vec<Site>),
}

#[derive(Debug, Deserialize)]
struct Meta {
    rc: String,
    count: Option<usize>,
    msg: Option<String>,
}

#[derive(Debug, Deserialize)]
struct Site {
    name: String,
    desc: String,
}

#[derive(Debug, Deserialize)]
pub struct Device {
    pub device_id: String,
    pub mac: MacAddress,
    pub ip: Option<Ipv4Addr>,
    pub r#type: String,
    pub name: Option<String>,
    pub model: String,
    pub state: usize,
    // uplink_ap_mac isn't just null or a MAC address, it may also be "" 🤦
    #[serde(default, deserialize_with = "deserialize_err_to_default")]
    pub uplink_ap_mac: Option<MacAddress>,
    pub gateway_mac: Option<MacAddress>,
    pub last_uplink: Option<Uplink>,
    // We don't have any devices on the NYC Mesh network at the moment that don't have any WiFi
    // capabilities at all, so putting this in as an Option for now, however, this property can be
    // 0, as seen with a UniFi smart plug
    pub wifi_caps: Option<usize>,
    #[serde(skip)]
    pub site: Option<String>,
}

#[derive(Debug, Deserialize)]
pub struct Uplink {
    pub uplink_mac: MacAddress,
}
