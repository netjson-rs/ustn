use std::collections::HashMap;

use futures::{stream, StreamExt};
use netjson_networkgraph::Address;

use crate::*;

#[derive(Deserialize)]
pub struct Uisp {
    pub uri: String,
    pub invalid_certs: Option<bool>,
    pub username: String,
    pub password: String,
    #[serde(skip)]
    pub token: Option<Vec<u8>>,
    pub token_path: Option<PathBuf>,
    #[serde(skip)]
    client: Option<reqwest::Client>,
}

#[async_trait]
impl DataSource for Uisp {
    async fn login(&mut self, retry: bool) -> Result<()> {
        let mut headers = header::HeaderMap::new();
        headers.insert(
            reqwest::header::ACCEPT,
            header::HeaderValue::from_static("application/json"),
        );
        headers.insert(
            "x-auth-token",
            match (&self.token, retry) {
                (Some(t), false) => Ok(header::HeaderValue::from_bytes(&t)?),
                _ => {
                    let mut login_uri = self.uri.clone();
                    login_uri.push_str("user/login");
                    let login = build_client(self, headers.clone())
                        .await?
                        .post(&login_uri)
                        .form(&[
                            ("password", self.password.as_ref()),
                            ("username", self.username.as_ref()),
                            ("sessionTimeout", "604800000"),
                        ])
                        .send()
                        .await?;

                    if login.status() != reqwest::StatusCode::OK {
                        return Err(Box::new(Error::Auth(login_uri)));
                    }

                    match login.headers().get("x-auth-token") {
                        Some(t) => {
                            config::set_token(t.as_bytes(), self)?;
                            Ok(t.clone())
                        }
                        None => Err(Box::new(Error::Unexpected(
                            "200 OK but no token?!".into(),
                            login_uri,
                        ))),
                    }
                }
            }?,
        );

        self.client = Some(build_client(self, headers).await?);
        Ok(())
    }

    async fn get_devices(&self) -> Result<Devices> {
        let client = self
            .client
            .as_ref()
            .ok_or_else(|| Error::Auth(self.uri.clone()))?;

        let mut devices_uri = self.uri.clone();
        devices_uri.push_str("devices");

        let response = client.get(&devices_uri).send().await?;

        let response_text = &response.text().await?;

        let mut devices: Vec<Device> = serde_json::from_str(response_text)
            .map_err(|e| Error::Deserialize(e, devices_uri.clone(), response_text.clone()))?;

        // Check if there's a discrepancy between reported stations and stations we have already
        // captured with our request

        // id: (reported, captured)
        let mut ap_stations: HashMap<&str, (u16, u16)> = HashMap::new();

        for device in &devices {
            let ident = &device.ident.as_ref();

            if &device.overview.as_ref().unwrap().status != "active"
                || ident.unwrap().model.is_some()
                    && ident.unwrap().model.as_ref().unwrap().starts_with("ACB")
            {
                continue;
            }
            if let Some(wireless_mode) = &device.overview.as_ref().unwrap().wireless_mode {
                if wireless_mode.starts_with("ap") {
                    let count = device
                        .overview
                        .as_ref()
                        .unwrap()
                        .stations_count
                        .unwrap_or(0);
                    if count > 0 {
                        match ap_stations.get_mut(ident.unwrap().id.as_str()) {
                            Some((rep, _)) => *rep = count,
                            None => {
                                ap_stations.insert(ident.unwrap().id.as_str(), (count, 0));
                            }
                        }
                    }
                    continue;
                }
            }
            if let Some(attr) = &device.attr {
                if let Some(ap) = &attr.ap_device {
                    match ap_stations.get_mut(ap.id.as_str()) {
                        Some((_, cap)) => *cap += 1,
                        None => {
                            ap_stations.insert(ap.id.as_str(), (0, 1));
                        }
                    }
                }
            }
        }

        let stragglers: Vec<_> = ap_stations
            .iter()
            .filter_map(|(ap_id, (rep, cap))| match rep > cap {
                true => Some(format!(
                    "{}/{}/detail?withStations=true",
                    devices_uri, ap_id
                )),
                false => None,
            })
            .collect();

        // The following may seem a bit weird, in particular sending out requests to fetch all APs
        // simultaneously, but it speeds up the operations significantly
        let stragglers_len = stragglers.len();
        let mut straggler_responses = stream::iter(stragglers)
            .map(|uri| {
                let client = client.clone();
                async move {
                    match client.get(uri.clone()).send().await {
                        Ok(resp) => Ok((uri, resp.text().await)),
                        Err(e) => Err(e),
                    }
                }
            })
            .buffer_unordered(stragglers_len);

        while let Some(response) = straggler_responses.next().await {
            let (uri, response_result) = response?;

            let response_text = response_result?;

            let ap: ApWithStragglers = serde_json::from_str(&response_text)
                .map_err(|e| Error::Deserialize(e, uri, response_text))?;
            let ap_id = ap.ident.id.clone();
            let ap_hostname = ap.ident.hostname.unwrap().clone();
            let mut stations: Vec<_> = ap
                .interfaces
                .into_iter()
                .filter_map(|iface| {
                    iface.stations.map(|stations| {
                        stations
                            .into_iter()
                            .filter_map(|mut sta| {
                                if sta.ident.is_some() || !sta.connected.unwrap() {
                                    return None;
                                }
                                sta.attr = Some(DeviceAttributes {
                                    ap_device: Some(DeviceAp {
                                        id: ap_id.clone(),
                                        name: ap_hostname.clone(),
                                    }),
                                    ssid: None,
                                });
                                Some(sta)
                            })
                            .collect::<Vec<_>>()
                    })
                })
                .flatten()
                .collect();
            devices.append(&mut stations);
        }

        Ok(Devices::Uisp(devices))
    }

    fn certs(&self) -> &Option<bool> {
        &self.invalid_certs
    }
}

#[derive(Debug, Deserialize)]
pub struct Device {
    #[serde(rename = "attributes")]
    pub attr: Option<DeviceAttributes>,
    // only for "stragglers" ?withStations=true
    pub connected: Option<bool>,
    // identification /devices
    // deviceIdentification "stragglers" ?withStations=true
    #[serde(rename = "identification")]
    #[serde(alias = "deviceIdentification")]
    pub ident: Option<DeviceIdentification>,
    pub overview: Option<DeviceOverview>,
    #[serde(rename = "ipAddress")]
    pub ip_address: Option<Address>,
    // mac and name at this level are only in stations parsed from ?withStations=truestragglers
    pub mac: Option<Address>,
    pub name: Option<String>,
    #[serde(rename = "rxSignal")]
    pub rx_signal: Option<i16>,
    #[serde(rename = "txSignal")]
    pub tx_signal: Option<i16>,
}

#[derive(Debug, Deserialize)]
pub struct DeviceAttributes {
    #[serde(rename = "apDevice")]
    pub ap_device: Option<DeviceAp>,
    pub ssid: Option<String>,
}

#[derive(Debug, Deserialize)]
pub struct DeviceAp {
    pub id: String,
    pub name: String,
}

#[derive(Debug, Deserialize)]
pub struct DeviceIdentification {
    pub id: String,
    // Always set for /devices, not set for deviceIndentification in ?withStations=true
    pub hostname: Option<String>,
    pub mac: Option<Address>,
    pub model: Option<String>,
    #[serde(rename = "modelName")]
    pub model_name: String,
    #[serde(rename = "firmwareVersion")]
    pub firmware: Option<String>,
    pub r#type: String,
}

#[derive(Debug, Deserialize)]
pub struct DeviceOverview {
    pub status: String,
    pub frequency: Option<u32>,
    #[serde(rename = "wirelessMode")]
    pub wireless_mode: Option<String>,
    #[serde(rename = "stationsCount")]
    pub stations_count: Option<u16>,
}

#[derive(Debug, Deserialize)]
struct ApWithStragglers {
    #[serde(rename = "identification")]
    pub ident: DeviceIdentification,
    interfaces: Vec<Interface>,
}

#[derive(Debug, Deserialize)]
pub struct Interface {
    stations: Option<Vec<Device>>,
}
