use std::error::Error as StdError;
use std::{fmt, result};

pub type Result<T> = result::Result<T, Box<dyn StdError + Send + Sync>>;

#[derive(Debug)]
pub enum Error {
    Auth(String),
    Config(&'static str),
    Empty,
    Deserialize(serde_json::Error, String, String),
    Unexpected(String, String),
}

impl StdError for Error {}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        use Error::*;
        match self {
            Auth(uri) => write!(f, "{}: Unauthorized", uri),
            Config(e) => write!(f, "Configuration error: {}", e),
            Empty => write!(f, "Empty graph"),
            Deserialize(e, uri, response) => write!(f, "{}: {}\n{}", uri, e, response),
            Unexpected(e, uri) => write!(f, "{}: Unexpected response\n{}", uri, e),
        }
    }
}
