use std::collections::HashMap;
use std::path::{Path, PathBuf};

use async_trait::async_trait;
use futures::{stream, StreamExt};
use netjson_networkgraph::*;
use regex::Regex;
use reqwest::header;
use serde::Deserialize;
use serde_json::{map::Map, Value};

pub mod config;
pub use config::Config;
pub mod error;
pub use error::*;

pub mod uisp;
pub mod unifi;

static USER_AGENT: &str = concat!(env!("CARGO_PKG_NAME"), "/", env!("CARGO_PKG_VERSION"),);

async fn build_client<D>(source: &D, headers: header::HeaderMap) -> Result<reqwest::Client>
where
    D: DataSource,
{
    let mut client = reqwest::Client::builder()
        .user_agent(USER_AGENT)
        .default_headers(headers);
    if let Some(invalid_cert) = source.certs() {
        client = client.danger_accept_invalid_certs(*invalid_cert);
    }
    Ok(client.build()?)
}

#[async_trait]
pub trait DataSource {
    async fn login(&mut self, retry: bool) -> Result<()>;
    async fn get_devices(&self) -> Result<Devices>;
    fn certs(&self) -> &Option<bool>;
}

#[derive(Debug)]
pub enum Devices {
    Uisp(Vec<uisp::Device>),
    UniFi(Vec<unifi::Device>),
}

impl Devices {
    pub fn into_graph<S>(
        self,
        all: bool,
        wireless: bool,
        filter: &Option<S>,
    ) -> Result<NetworkGraph>
    where
        S: AsRef<str>,
    {
        let filter = match filter {
            Some(ap) => Some(Regex::new(ap.as_ref())?),
            None => None,
        };

        macro_rules! add_property {
            ($properties:expr, $key:expr, $value:expr) => {
                $properties.insert($key.into(), Value::String($value.into()));
            };
        }

        let mut links: Vec<Link> = vec![];
        let mut nodes: Vec<Node> = vec![];

        match self {
            Devices::Uisp(devices) => {
                let mut device_ids: Vec<String> = vec![];

                let devices: Vec<uisp::Device> = devices
                    .into_iter()
                    .filter(|device| {
                        if (all
                            || !all
                                && (device.overview.is_some()
                                    && &device.overview.as_ref().unwrap().status == "active")
                            || (device.connected.is_some() && device.connected.unwrap()))
                            && (match &filter {
                                Some(filter) => {
                                    device.attr.is_some()
                                        && device.attr.as_ref().unwrap().ap_device.is_some()
                                        && filter.is_match(
                                            &device
                                                .attr
                                                .as_ref()
                                                .unwrap()
                                                .ap_device
                                                .as_ref()
                                                .unwrap()
                                                .name
                                                .as_str(),
                                        )
                                }
                                None => true,
                            })
                            && (!all
                                && !wireless
                                && ((device.ident.is_some()
                                    && &device.ident.as_ref().unwrap().r#type != "blackBox")
                                    || (device.ident.is_none() && device.connected.is_some()))
                                || !all
                                    && wireless
                                    && ((device.ident.is_some()
                                        && device
                                            .ident
                                            .as_ref()
                                            .unwrap()
                                            .r#type
                                            .starts_with("air"))
                                        || device.ident.is_none()
                                            && device.connected.is_some()
                                            && device.connected.unwrap()))
                        {
                            // Track which devices are included in this vector so that we can create
                            // valid Link output
                            match &device.ident {
                                Some(ident) => device_ids.push(ident.id.clone()),
                                None => {
                                    device_ids.push(format!("{}", device.mac.as_ref().unwrap()))
                                }
                            }
                            true
                        } else {
                            false
                        }
                    })
                    .collect();
                // Make sure we only include links to devices that are actually part of this output,
                // otherwise the output would be invalid
                for device in &devices {
                    let mut props: Map<String, Value> = Map::new();

                    // Make exceptions for airCubes because we don't want to leak details about
                    // home routers, be it SSID, frequency or
                    let mut aircube = false;

                    let mut node = match &device.ident {
                        Some(ident) => {
                            let mut node =
                                Node::new(&ident.id).set_label(ident.hostname.as_ref().unwrap());
                            if let Some(mac) = &ident.mac {
                                node.local_addresses.push(mac.clone());
                            }
                            add_property!(props, "model_name", &ident.model_name);
                            add_property!(props, "type", &ident.r#type);
                            if let Some(model) = &ident.model {
                                aircube = model.starts_with("ACB");
                                add_property!(props, "model", model);
                            }
                            if let Some(firmware) = &ident.firmware {
                                add_property!(props, "firmware", firmware);
                            }
                            node
                        }
                        None => {
                            let mut node = Node::new(&format!("{}", &device.mac.as_ref().unwrap()));
                            if let Some(name) = &device.name {
                                node = node.set_label(name);
                            }
                            node.local_addresses
                                .push(device.mac.as_ref().unwrap().clone());
                            node
                        }
                    };
                    if let Some(ipaddr) = &device.ip_address {
                        node.local_addresses.push(ipaddr.clone());
                    }
                    if let (Some(overview), false) = (&device.overview, aircube) {
                        if let Some(wireless) = &overview.wireless_mode {
                            add_property!(props, "wireless_mode", wireless);
                            if wireless.starts_with("ap") {
                                if let Some(attr) = &device.attr {
                                    if let Some(ssid) = &attr.ssid {
                                        add_property!(props, "ssid", ssid);
                                    }
                                }
                            }
                        }
                        if let Some(frequency) = &overview.frequency {
                            props.insert(
                                "frequency".into(),
                                Value::Number(serde_json::Number::from(*frequency)),
                            );
                        }
                    }
                    node = node.set_properties(props);

                    nodes.push(node);

                    if let Some(attributes) = &device.attr {
                        if let Some(ap) = &attributes.ap_device {
                            if device_ids.contains(&ap.id) {
                                match &device.ident {
                                    Some(ident) => links.push(Link::new(&ident.id, &ap.id)),
                                    None => links.push(Link::new(
                                        &format!("{}", device.mac.as_ref().unwrap()),
                                        &ap.id,
                                    )),
                                }
                            }
                        }
                    }
                }

                Ok(NetworkGraph::new("UISP")
                    .set_nodes(nodes)
                    .set_links(links)?)
            }
            Devices::UniFi(devices) => {
                let mut device_map: HashMap<[u8; 6], String> = HashMap::new();

                let devices: Vec<_> = devices
                    .into_iter()
                    .filter(|device| {
                        if all
                            || (!all && device.state > 0)
                                && (match (&filter, &device.site, &device.name) {
                                    // Filter by site as well as name - this works with the way NYC
                                    // Mesh uses sites, some of which are general ones like "Park"
                                    // but are in a completely different location altogether, but
                                    // then a node number might be part of the device name
                                    (Some(filter), Some(site), Some(name)) => {
                                        filter.is_match(site) || filter.is_match(name)
                                    }
                                    (Some(filter), Some(site), _) => filter.is_match(site),
                                    (Some(filter), _, Some(name)) => filter.is_match(name),
                                    _ => true,
                                })
                                && (!all
                                    && (!wireless
                                        || wireless
                                            && match device.wifi_caps {
                                                Some(wifi_caps) => wifi_caps > 0,
                                                None => false,
                                            }))
                        {
                            device_map.insert(device.mac.bytes(), device.device_id.clone());
                            true
                        } else {
                            false
                        }
                    })
                    .collect();

                for device in &devices {
                    let mut addresses = vec![Address::Mac(device.mac)];
                    if let Some(ipv4addr) = device.ip {
                        addresses.push(Address::Ipv4(ipv4addr));
                    }
                    let mut node = Node::new(&device.device_id).set_local_addresses(addresses);

                    if let Some(name) = &device.name {
                        node = node.set_label(&name);
                    }

                    let mut props: Map<String, Value> = Map::new();
                    add_property!(props, "model_name", &device.model);
                    add_property!(props, "type", &device.r#type);
                    if let Some(site) = &device.site {
                        add_property!(props, "site", site);
                    }
                    node = node.set_properties(props);
                    nodes.push(node);

                    let mut link = false;

                    // Seems to be primarily used for WiFi meshing?
                    if let Some(uplink_ap_mac) = &device.uplink_ap_mac {
                        if let Some(device_id) = device_map.get(&uplink_ap_mac.bytes()) {
                            links.push(Link::new(&device.device_id, &device_id));
                            link = true;
                        }
                    }

                    // Possibly mostly used when physically connected to a switch?
                    if let (false, Some(last_uplink)) = (link, &device.last_uplink) {
                        if let Some(device_id) = device_map.get(&last_uplink.uplink_mac.bytes()) {
                            links.push(Link::new(&device.device_id, &device_id));
                            link = true;
                        }
                    }

                    // Last resort, is there a UniFi gateway we're hooked up to somehow?
                    if let (false, Some(gateway_mac)) = (link, &device.gateway_mac) {
                        if let Some(device_id) = device_map.get(&gateway_mac.bytes()) {
                            links.push(Link::new(&device.device_id, &device_id));
                        }
                    }
                }

                Ok(NetworkGraph::new("UniFi")
                    .set_nodes(nodes)
                    .set_links(links)?)
            }
        }
    }
}

async fn grab_with<D: ?Sized>(
    sources: Vec<&mut D>,
    all: bool,
    wireless: bool,
    filter: &Option<String>,
) -> Result<NetworkGraph>
where
    D: DataSource,
{
    let sources_len = sources.len();
    let mut workers = stream::iter(sources)
        .map(|source| async move {
            let mut retry = 0usize;
            while retry < 2 {
                // If the first login fails with Error::Auth, we want to return it
                source.login(retry > 0).await?;
                // If a subsequent request fails with Error::Auth, force retry to make sure it's not
                // due to outdated cookies or tokens
                match source.get_devices().await {
                    Ok(d) => return Ok(d),
                    Err(e) => {
                        if let Some(&Error::Auth(_)) = e.downcast_ref() {
                            retry += 1;
                        } else {
                            return Err(e);
                        }
                    }
                }
            }
            Err(Box::new(Error::Auth("".into())))
        })
        .buffer_unordered(sources_len);

    let mut graph: Option<NetworkGraph> = None;
    while let Some(output) = workers.next().await {
        match &mut graph {
            Some(graph) => graph.merge(output?.into_graph(all, wireless, filter)?)?,
            None => graph = Some(output?.into_graph(all, wireless, filter)?),
        }
    }

    graph.ok_or_else(|| Error::Empty.into())
}

pub async fn grab(config: &mut config::Config) -> Result<NetworkGraph> {
    let mut sources: Vec<&mut dyn DataSource> = vec![];

    if let Some(unifi_controllers) = config.unifi.as_mut() {
        for unifi_controller in unifi_controllers {
            sources.push(unifi_controller);
        }
    }

    if let Some(uisp_controllers) = config.uisp.as_mut() {
        for uisp_controller in uisp_controllers {
            sources.push(uisp_controller);
        }
    }

    grab_with(sources, false, false, &None).await
}

pub async fn grab_custom(
    config: &mut config::Config,
    uisp: bool,
    unifi: bool,
    all: bool,
    wireless: bool,
    filter: &Option<String>,
) -> Result<NetworkGraph> {
    let mut sources: Vec<&mut dyn DataSource> = vec![];

    if let (Some(unifi_controllers), false) = (config.unifi.as_mut(), uisp) {
        for unifi_controller in unifi_controllers {
            sources.push(unifi_controller);
        }
    }

    if let (Some(uisp_controllers), false) = (config.uisp.as_mut(), unifi) {
        for uisp_controller in uisp_controllers {
            sources.push(uisp_controller);
        }
    }

    grab_with(sources, all, wireless, filter).await
}
